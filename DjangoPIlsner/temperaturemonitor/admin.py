from django.contrib import admin

from models import TemperatureGauge, TemperatureLog, TargetTemperature
# Register your models here.
admin.site.register(TemperatureGauge)
admin.site.register(TemperatureLog)
admin.site.register(TargetTemperature)