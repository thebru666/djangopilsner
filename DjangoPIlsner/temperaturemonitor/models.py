from django.db import models

# Create your models here.


class TemperatureGauge(models.Model):
    filesystem_id = models.CharField(max_length=50)
    nicename = models.CharField(max_length=100)

    def __unicode__(self):
        return self.nicename or self.filesystem_id

class TemperatureLog(models.Model):
    gauge = models.ForeignKey(TemperatureGauge)
    temperature = models.DecimalField(max_digits=5, decimal_places=3)
    added_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '%s @ %s' % (self.temperature, self.added_on)


class TargetTemperature(models.Model):
    temperature = models.DecimalField(max_digits=5, decimal_places=3)
    start = models.DateTimeField()
    end = models.DateTimeField(null=True)
    affects = models.ForeignKey(TemperatureGauge, blank=True, null=True)

    def __unicode__(self):
        return '%s (%s -> %s)' % (self.temperature, self.start, self.end)
