__author__ = 'sam'


from celery.utils.log import get_task_logger
from datetime import datetime
from celery.decorators import task
from temperaturereader import readtemperatures


logger = get_task_logger(__name__)

@task
def check_temperature():
    logger.info("Starting checking temperatures...")
    readtemperatures()
    logger.info("Completed check.")
    return 'Winning.'

