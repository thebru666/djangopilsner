__author__ = 'sam'

import os
import re
from decimal import Decimal
from models import TemperatureLog, TemperatureGauge


SOURCEPATH = '/sys/bus/w1/devices/'
SOURCEFILENAME = 'w1_slave'
SEARCHRE = re.compile('^.*t=(\d+)\n$')


class TemperatureReader(object):

    def __init__(self, gauge):
        self.gauge = gauge
        self.temperature = None
        self.readfilename = os.path.join(SOURCEPATH, gauge.filesystem_id, SOURCEFILENAME)

    def read(self):
        """

        """
        filehandle = open(self.readfilename, 'r')
        line1 = filehandle.readline()
        line2 = filehandle.readline()
        filehandle.close()
        if not line1.endswith('YES\n'):
            raise ValueError('Invalid data received')
        # otherwise, we're good!
        result = SEARCHRE.match(line2)
        if not result:
            raise ValueError('Could not parse line : %s', line2)
        self.temperature = Decimal(result.group(1))

    def save(self):
        if self.temperature is not None:
            newLog = TemperatureLog(
                gauge=self.gauge,
                temperature=self.temperature / 1000
            )
        newLog.save()


def readtemperatures():
    for gauge in TemperatureGauge.objects.all():
        reader = TemperatureReader(gauge)
        try:
            reader.read()
        except ValueError:
            # XXX logger?
            raise
        else:
            reader.save()
