__author__ = 'sam'

from models import InstrumentState
from datetime import datetime
import os
import subprocess
import logging

logger = logging.getLogger(__file__)

class PowerManager(object):

    def __init__(self, instrument):
        self.instrument = instrument

    def _update_state(self, state):
        InstrumentState.objects.filter(instrument=self.instrument, end=None).update(end=datetime.now())
        newState = InstrumentState(
            instrument=self.instrument,
            state=state,
            start=datetime.now(),
        )
        newState.save()

    def get_state(self):
        state = InstrumentState.objects.filter(instrument=self.instrument, end=None).all()[0].state
        logger.debug('Found state %s', state)
        return state

    def get_last_change(self):
        change = InstrumentState.objects.filter(instrument=self.instrument, end=None).all()[0].start
        logger.debug('Found last state change %s', change)
        return change



    def _get_power_command(self):
        # to begin with, atleast, there shoudl be an onneroffer.py in this
        # directory.  Return that to run.
        # Needs to be configured in sudoers
        # XXX this should be stateful for each instrument.  I only have on right now....
        thisDir = os.path.dirname(os.path.realpath(__file__))
        script = os.path.join(thisDir, 'onneroffer.py')
        return ['sudo', 'python', script]


    def turnon(self, force=False):
        if self.get_state() == 'on' and not force:
            logger.warn('Tried to turn on already on state.')
            return
        # do the actual power call.
        logger.info('Flicking switch.')
        subprocess.Popen(self._get_power_command())
        # then update the database
        logger.info('Updating db to on')
        self._update_state('on')


    def turnoff(self, force=False):
        if self.get_state() == 'off' and not force:
            logger.warn('Tried to turn off already off state.')
            return
        # do the actual power call.
        logger.info('Flicking switch.')
        subprocess.Popen(self._get_power_command())
        # then update the database
        logger.info('Updating db to off')
        # then update the database
        self._update_state('off')
