__author__ = 'sam'

import logging
import RPi.GPIO as GPIO
import time


logger = logging.getLogger(__name__)

class OnOffer(object):

    def __init__(self):
        # config this
        self.pin = 22

    def _prepare(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(22, GPIO.OUT)

    def _cleanup(self):
        GPIO.cleanup()

    def _signal(self, val):
        GPIO.output(self.pin, val)

    def _signalOn(self):
        self._signal(True)

    def _signalOff(self):
        self._signal(False)

    def sendSignal(self, delay=2.5):
        logger.info('Turning %s on for %s', self.pin, delay)
        self._prepare()
        self._signalOn()
        time.sleep(delay)
        self._signalOff()
        self._cleanup()
        logger.info('Turned off %s', self.pin)


if __name__ == '__main__':
    OnOffer().sendSignal()
