
"""
The smarts!

Check our recent temperatures and decide if we want to turn on/off a heater, cooler, etc.

http://www.elcojacobs.com/controlling-fridge-and-beer-temperature-with-a-predictive-onoff-algorithm-and-pid/

"""

from models import Instrument
from temperaturemonitor.models import TargetTemperature
import datetime
from django.db.models import Q
from powermanager import PowerManager
from decimal import Decimal
import logging

logger = logging.getLogger(__name__)

class TemperatureBroker(object):

    def check(self):
        """
        Check if we're at the right temperature.
        """
        # find out what our current target temperature is
        now = datetime.datetime.now()
        target_temperatures = TargetTemperature.objects.filter(
            Q(start__lte=now, end__gt=now) | Q(start__lte=now, end__isnull=True)
        ).all()
        for target_temperature in target_temperatures:
            # a temperature can be aimed at multiple gauges.
            gauge = target_temperature.affects
            current_temp_read = gauge.temperaturelog_set.order_by('-added_on')[0]
            current_temp = current_temp_read.temperature
            logger.info('Checking %s.  Currently at %s - should be %s',
                gauge.nicename, current_temp, target_temperature.temperature)
            # right, we have a temperature, and an aimed temperature.
            # is not right?
            for instrument in gauge.instrument_set.all():
                # XZXXX THIS IS WHERE SMART THINGS GO!
                pm = PowerManager(instrument)
                last_change = pm.get_last_change() 
                if last_change + datetime.timedelta(minutes=30) > now:
                    logger.info('Not performing any changes as last change was less than 30 mins ago (%s)',
                        last_change)
                    continue
                # Too cold?
                # XXXX allowed variance of .5
                if current_temp < (target_temperature.temperature):
                    if instrument.instrumenttype == 'cooler' and pm.get_state() == 'on':
                        # wtf turn that shit off.
                        # XXX
                        logger.info('Turning off cooler %s due to '
                            'temperature of %s on %s (needs to be %s)',
                            instrument.instrumentname, current_temp, gauge.nicename,
                            target_temperature.temperature
                            )
                    elif instrument.instrumenttype == 'heater' and pm.get_state() == 'off':
                        logger.info('Turning  on heater %s due to '
                            'temperature of %s on %s (needs to be %s)',
                            instrument.instrumentname, current_temp, gauge.nicename,
                            target_temperature.temperature
                            )
                        pm.turnon()
                # Too hot?
                elif current_temp > (target_temperature.temperature + Decimal('0.5')):
                    if instrument.instrumenttype == 'cooler' and pm.get_state() == 'off':
                        # wtf turn that shit off.
                        # XXX
                        logger.info('Turning on cooler %s due to '
                            'temperature of %s on %s (needs to be %s)',
                            instrument.instrumentname, current_temp, gauge.nicename,
                            target_temperature.temperature
                            )
                    elif instrument.instrumenttype == 'heater' and pm.get_state() == 'on':
                        logger.info('Turning off heater %s due to '
                            'temperature of %s on %s (needs to be %s)',
                            instrument.instrumentname, current_temp, gauge.nicename,
                            target_temperature.temperature
                            )
                        pm.turnoff()
                else:
                    # just right
                    if pm.get_state() == 'on':
                        logger.info('Turning off %s due to '
                            'temperature of %s on %s (needs to be %s)',
                            instrument.instrumentname, current_temp, gauge.nicename,
                            target_temperature.temperature
                            )
                        if instrument.instrumenttype == 'heater':
                            pm.turnoff()
                    else:
                        # xxx smarts to go "the last turn off/onn didn't work!"
                        logger.info('Not doing a thing - current state of %s is %s',
                            instrument.instrumentname, pm.get_state()
                            )

