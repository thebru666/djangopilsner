from django.contrib import admin

from models import Instrument, InstrumentState

# Register your models here.
admin.site.register(InstrumentState)
admin.site.register(Instrument)
