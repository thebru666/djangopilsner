__author__ = 'sam'


from celery.utils.log import get_task_logger
from celery.decorators import task
from temperaturebroker import TemperatureBroker


logger = get_task_logger(__name__)

@task
def check_powermanagement():
    logger.info("Starting check for power management...")
    TemperatureBroker().check()
    logger.info("Completed check.")

