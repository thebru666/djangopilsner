from django.db import models

from temperaturemonitor.models import TemperatureGauge
# Create your models here.

class Instrument(models.Model):
    instrumentname = models.CharField(max_length=100)
    # heater / cooler
    instrumenttype = models.CharField(max_length=10)
    rfsignal = models.CharField(max_length=50, null=True, blank=True)
    affects = models.ForeignKey(TemperatureGauge, blank=True, null=True)


    def __unicode__(self):
        return self.instrumentname

class InstrumentState(models.Model):
    instrument = models.ForeignKey(Instrument)
    start = models.DateTimeField()
    end = models.DateTimeField(null=True, blank=True)
    state = models.CharField(max_length=10)

    def __unicode__(self):
        return '%s (%s -> %s)' % (self.state, self.start, self.end)

