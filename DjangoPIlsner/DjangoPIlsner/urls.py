from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
admin.autodiscover()

urls = ['',
    # Examples:
    # url(r'^$', 'DjangoPIlsner.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^tempgraph/', 'DjangoPIlsner.views.tempgraph'),
    url(r'^graphdata/', 'DjangoPIlsner.views.graphdata', name='graphdata'),
    ] +  static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns = patterns(*urls)

