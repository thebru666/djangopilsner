"""
Django settings for DjangoPIlsner project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
TEMPLATE_DIRS = ( os.path.join(BASE_DIR, 'templates'), )
PROJECT_DIR = os.path.dirname(__file__)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '4b$#fv=pjwx!nv^nawy2%be5r_))q^7n_um+jx1e3ghj*z!6z@'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'south',
    'djcelery',
    'temperaturemonitor',
    'powermanager'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'DjangoPIlsner.urls'

WSGI_APPLICATION = 'DjangoPIlsner.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
# stupid timeout - we don't care if it takes ages, just write it damnit.
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        'OPTIONS' : {
            'timeout' : 30
	} 	
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Australia/Melbourne'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATICFILES_DIRS = (os.path.join(PROJECT_DIR, 'static'),)

# The default Django db scheduler
from datetime import timedelta
CELERY_ACCEPT_CONTENT = ['json', 'pickle',]

#CELERY_RESULT_BACKEND = "amqp"
CELERY_IMPORTS = ("temperaturemonitor.tasks", "powermanager.tasks")
CELERY_ALWAYS_EAGER = True
CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"
CELERYBEAT_SCHEDULE = {
    "read-temperatures": {
        "task": "temperaturemonitor.tasks.check_temperature",
        #Every minute
        "schedule": timedelta(minutes=2),
        "args": (),
    },
    "check-power" : {
         "task" : "powermanager.tasks.check_powermanagement",
         "schedule": timedelta(minutes=2),
         "args" : (),
    } 
}

