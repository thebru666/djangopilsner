import json
import time

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.db.models import Q
from django.contrib.auth.decorators import login_required


from temperaturemonitor.models import TemperatureLog, TemperatureGauge, TargetTemperature
from powermanager.models import Instrument, InstrumentState


# Create your views here.
@login_required
def tempgraph(request):
    onid = request.GET.get('on', None)
    if onid:
	from powermanager.powermanager import PowerManager
        instrument = Instrument.objects.get(pk=onid)
        pm = PowerManager(instrument)
        pm.turnon(force=True)
        return redirect('/tempgraph/')
    offid = request.GET.get('off', None)
    if offid:
        from powermanager.powermanager import PowerManager
        instrument = Instrument.objects.get(pk=offid)
        pm = PowerManager(instrument)
        pm.turnoff(force=True)
        return redirect('/tempgraph/')

    return render(request, 'tempgraph.html', { 'instruments' : Instrument.objects.all()})

def _formatTime(timetoformat):
    # boo - + 10 hours, cos fuck timezones.
    return (time.mktime(timetoformat.timetuple()) + 10 * 60 * 60) * 1000

def _formatDecimal(number):
    return float('%s' % number)

def graphdata(request):

    goback = int(request.GET.get('goback', 1000))


    datasets = []
    firstTime = None
    lastTime = None
    minTemp = 1000
    for gauge in TemperatureGauge.objects.all():
        temperatures = []
        for templog in TemperatureLog.objects.filter(
                gauge=gauge).order_by('-added_on').all()[:goback]:
            firstTime = templog.added_on
            lastTime = lastTime or templog.added_on
            minTemp = min(templog.temperature, minTemp)
            temperatures.append(
                [_formatTime(templog.added_on), float(str(templog.temperature)) ])
        label = '%s = 0' % (gauge.nicename or gauge.filesystem_id)
        datasets.append( {'data' : temperatures, 'label' : label,
             'lines' : { 'show' : True },
             'points' : { 'show' : False }} )

    targets = {}
    # find those that started at the start of this time block.
    for targetTemp in TargetTemperature.objects.filter(
            start__lt=firstTime).filter(
            Q(end__gte=firstTime) | Q(end__isnull=True)).all():
        thisTarget = targets.setdefault(targetTemp,
            {'label' : 'Target for %s: %s' % (targetTemp.affects.nicename, targetTemp.temperature),
             'data' : []})
        thisTarget['data'].append(
            [_formatTime(firstTime), _formatDecimal(targetTemp.temperature)] )

    # now find aeverything taht started during this time block.
    for targetTemp in TargetTemperature.objects.filter(
            start__gt=firstTime).all():
        thisTarget = targets.setdefault(targetTemp,
            {'label' : 'Target for %s: %s' % (targetTemp.affects.nicename, targetTemp.temperature),
             'data' : []})
        thisTarget['data'].append(
            [_formatTime(targetTemp.start), _formatDecimal(targetTemp.temperature)] )

    # now put an 'end' line for any that finished during this time block, or go to the end.
    for targetTime, thisTarget in targets.items():
        if targetTime.end < lastTime:
            thisTarget['data'].append(
                [_formatTime(targetTime.end), _formatDecimal(targetTime.temperature) ] )
            thisTarget['data'].append( [ _formatTime(targetTime.end), None ])
        else:
            thisTarget['data'].append(
                [_formatTime(lastTime), _formatDecimal(targetTemp.temperature)] )

    datasets += targets.values()


    offLevel = 1
    onLevel = 5

    instruments = {}
    # find those that started at the start of this time block.
    for instrumentstate in InstrumentState.objects.filter(
            start__lt=firstTime).filter(
            Q(end__gte=firstTime) | Q(end__isnull=True)).all():
        thisState = instruments.setdefault(instrumentstate.instrument,
            {'label' : '%s on/off' % instrumentstate.instrument.instrumentname,
             'data' : [],
             'lines' : { 'show' : True, 'fill': True }})
        thisState['data'].append(
            [_formatTime(firstTime), instrumentstate.state == 'on' and onLevel or offLevel ] )

    # now find aeverything taht started during this time block.
    for instrumentstate in InstrumentState.objects.filter(
            start__gt=firstTime).filter(
            Q(end__lt=lastTime) | Q(end__isnull=True)).all():
        thisState = instruments.setdefault(instrumentstate.instrument,
            {'label' : '%s on/off' % instrumentstate.instrument.instrumentname,
             'data' : [],
             'lines' : { 'show' : True, 'fill': True }})
        # if we have a previous state
        if thisState['data']:
            # put and end point of the same value here.
            thisState['data'].append(
                [_formatTime(instrumentstate.start), thisState['data'][-1][1] ] )
            # and a cutoff
            thisState['data'].append( None )

        thisState['data'].append(
            [_formatTime(instrumentstate.start), instrumentstate.state == 'on' and onLevel or offLevel ] )

    # now put an 'end' line for everything else.
    # no need to pick up on those that ended here - they should be replaced with a beginning :)
    for state, thisState in instruments.items():
        thisState['data'].append(
            [_formatTime(lastTime), instrumentstate.state == 'on' and onLevel or offLevel ] )

    datasets += instruments.values()


    to_json = {'data' :  datasets  }



    return HttpResponse(json.dumps(to_json), content_type='application/json')
